# Bienvenue sur le projet de WebServices 2019!

## Auteurs
* Antoine BERNIER
* Clément CLAPTIEN
* Nicolas LEURS. 

## Installation

### Prérequis : ce qu'il faut avoir installé
* **php** (version recommandée : php7.1, https://doc.ubuntu-fr.org/php)
* **composer** (https://doc.ubuntu-fr.org/composer)
* **mysql** (https://doc.ubuntu-fr.org/mysql)
* **phpmyadmin** (https://doc.ubuntu-fr.org/phpmyadmin)


### Créer un projet Symfony
Se placer dans le répertoire dans lequel on souhaite créer le dossier du projet et créer le projet avec **composer** :
```bash
composer create-project symfony/skeleton webservice2019
```

### Récupération des données sur GitLab
Il faut maintenant créer un répertoire temporaire pour récupérer les fichiers du projet que l'on va ensuite déplacer dans le dossier du projet symfony :
```bash
mkdir tmp
cd tmp
git clone https://gitlab.com/nleurs/webservice2019.git
```
On écrase maintenant le contenu de **webservice2019** par celui de **tmp/webservice2019**.
On pourra ensuite supprimer le dossier **tmp**.
### Installation des modules requis
On se replace dans le dossier du projet et on ajoute les modules requis avec **composer** :

```bash
cd webservice2019/
composer require symfony/form
composer require symfony/swiftmailer-bundle
composer require --dev symfony/profiler-pack
composer require symfony/monolog-bundle
composer require --dev symfony/debug-bundle
composer require sensio/framework-extra-bundle
composer require symfony/translation
composer require security
composer require symfony/security-bundle
composer require symfony/maker-bundle
composer require doctrine
composer require symfony/web-server-bundle
composer require symfony/twig-bundle
composer require symfony/validator
composer require --dev symfony/profiler-pack
composer require doctrine/doctrine-fixtures-bundle --dev
```
### Création de la base de données
Sous phpmyadmin ou via mysql, créer une base de données nommée **webservice2019**.

### Modification du fichier d'environnement
Il faut maintenant permettre l'accès de la base de données au webservice. Pour cela ouvrir le fichier **.env** présent dans le dossier **webservice2019** et modifier la ligne :
```
DATABASE_URL=mysql://db_user:db_password@127.0.0.1:3306/db_name
```
> Remplacer **db_user**, **db_password** et **db_name** par  respectivement **l'identifiant** et **le mot de passe** et enfin **le nom de la base _(webservice2019)_**.

### Mise à jour de la base
Il faut maintenant créer les tables nécessaires au bon fonctionnement du webservice via la commande :
```bash
php bin/console doctrine:schema:update --force
```

## Ajout de données dans la base
php bin/console doctrine:fixtures:load

## Lancement du webservice
Il suffit de taper la commande suivante :
```bash
php bin/console server:run
```