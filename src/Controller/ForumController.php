<?php
/**
 * Created by PhpStorm.
 * User: leurs
 * Date: 28/05/2019
 * Time: 10:25
 */

namespace App\Controller;
use Symfony\Component\Routing\Annotation\Route;

use App\Services\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ForumController extends AbstractController
{

    /**
     * @var GameService
     */
    private $gameService;

    public function __construct(GameService $gameService)
    {

        $this->gameService = $gameService;
    }

    /**
     * @Route("/", name="index")
     */
    public function index()
    {
        return $this->render('forum/index.html.twig');
    }
}