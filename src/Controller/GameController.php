<?php

namespace App\Controller;

use App\Entity\Game;
use App\Form\addGameType;
use App\Form\GameType;

use App\Services\GameService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class GameController extends AbstractController
{
    /**
     * @var GameService
     */
    private $gameService;

    public function __construct(GameService $gameService)
    {

        $this->gameService = $gameService;
    }

    /**
     * @Route("/getAllGame", name="getAllGame")
     */
    public function getAllGame()
    {
        $liste = $this->gameService->getAllGame();
        return $this->render('game/showGames.html.twig',
            ['liste' => $liste]);
    }

    /**
     * @Route("/getGame/{id}", name="getGame")
     */
    public function getGame($id=null)
    {
        $game = $this->gameService->getGame($id);
        return $this->render('game/showGame.html.twig',
            ['game' => $game]);
    }


    /**
     * @Route("/postGame", name="postGame")
     */
    public function postGame(Request $request)
    {
        $game = new Game();
        $formGame = $this->createForm(GameType::class, $game);

        $formGame->handleRequest($request);

        if ($formGame->isSubmitted() && $formGame->isValid()) {
            $this->gameService->postGame($game);
            $id=$game->getId();
            return $this->redirect("/getGame/$id");
        }
        return $this->render('game/postGame.html.twig', [
            'controller_name' => 'GameController',
            'formGame' => $formGame->createView(),
        ]);
    }


    /**
     * @Route("/addGame/{gameName}", name="addGame")
     */
    public function addGame(Request $request,$gameName)
    {
        $game = new Game();
        $game->setName($gameName);
        $formGame = $this->createForm(addGameType::class, $game);

        $formGame->handleRequest($request);

        if ($formGame->isSubmitted() && $formGame->isValid()) {
            $this->gameService->postGame($game);
            $id=$game->getId();
            return $this->redirect("/getGame/$id");
        }
        return $this->render('game/postGame.html.twig', [
            'controller_name' => 'GameController',
            'formGame' => $formGame->createView(),
        ]);
    }

    /**
     * @Route("/deleteGame/{id}", name="deleteGame")
     */
    public function deleteGame($id = null)
    {
        $this->gameService->delete($id);
        return $this->redirect("/getAllGame");
    }

    /**
     * @Route("/putGame/{id}", name="putGame")
     */
    public function putGame($id = null, Request $request)
    {
        $game = $this->gameService->getGame($id);
        $formGame = $this->createForm(GameType::class, $game);

        $formGame->handleRequest($request);

        if ($formGame->isSubmitted() && $formGame->isValid()) {
            $this->gameService->postGame($game);
            return $this->redirect("/getAllGame");
        }

        return $this->render('game/postGame.html.twig', [
            'controller_name' => 'GameController',
            'formGame' => $formGame->createView(),
        ]);
    }

    /**
     * @Route("/searchGame", name="searchGame")
     */
    public function searchGame(){
        $name = $_POST['name'];
        $game=$this->gameService->searchGame($name);
        return $this->render('game/showGame.html.twig', [
            'game' => $game,
            'name' => $name,
            ]);
    }

}
