<?php

namespace App\Controller;

use App\Entity\Game;
use App\Entity\Message;
use App\Form\MessageType;
use App\Services\GameService;
use App\Services\MessageService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class MessageController extends AbstractController
{
    /**
     * @var MessageController
     */
    private $messageService;

    /**
     * @var MessageController
     */
    private $gameService;

    public function __construct(MessageService $messageService, GameService $gameService)
    {

        $this->messageService = $messageService;
        $this->gameService = $gameService;
    }

    /**
     * @Route("/getAllMessages", name="getAllMessages")
     */
    public function getAllMessages()
    {
        $liste = $this->messageService->getAllMessages();
        return $this->render('message/index.html.twig',
            ['liste' => $liste]);
    }

    /**
     * @Route("/getMessage/{id}", name="getMessage")
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getMessage($id = null)
    {
        $message = $this->messageService->getMessage($id);
        return $this->render('message/showMessage.html.twig',
            ['message' => $message]);
    }


    /**
     * @Route("/postMessage", name="postMessage")
     * @Route("/postMessage/{idGame}", name="postMessage")
     * @param null $idGame
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function postMessage($idGame = null, Request $request)
    {
        $message = new Message();

        if ($idGame == null) {

            $formMessage = $this->createForm(MessageType::class, $message, [
                'game_service' => $this->gameService
            ]);

            $formMessage->handleRequest($request);

            if ($formMessage->isSubmitted() && $formMessage->isValid()) {
                $message->setSecurityUser($user = $this->get('security.token_storage')->getToken()->getUser());
                $this->messageService->postMessage($message);
                $idGame = $message->getGame()->getId();
                return $this->redirect("/getGame/$idGame");
            }
            return $this->render('message/postMessage.html.twig', [
                'controller_name' => 'MessageController',
                'formMessage' => $formMessage->createView(),
            ]);
        } else {
            $message->setText($_POST['message']);
            $message->setGame($this->gameService->getGame($idGame));
            $message->setSecurityUser($user = $this->get('security.token_storage')->getToken()->getUser());
            $this->messageService->postMessage($message);
            return $this->redirect("/getGame/$idGame");
        }
    }

    /**
     * @Route("/deleteMessage/{id}", name="deleteMessage")
     * @param null $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteMessage($id = null)
    {
        $this->messageService->delete($id);
        return $this->redirect("/getAllMessages");
    }

    /**
     * @Route("/putMessage/{id}", name="putMessage")
     * @param null $id
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function putMessage($id = null, Request $request)
    {
        $message = $this->messageService->getMessage($id);
        $formMessage = $this->createForm(MessageType::class, $message);

        $formMessage->handleRequest($request);

        if ($formMessage->isSubmitted() && $formMessage->isValid()) {
            $this->messageService->postMessage($message);
            return $this->redirect("/getAllMessages");
        }

        return $this->render('message/postMessage.html.twig', [
            'controller_name' => 'MessageController',
            'formMessage' => $formMessage->createView(),
        ]);
    }
}
