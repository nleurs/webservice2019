<?php

namespace App\Controller;

use App\Entity\SecurityUser;
use App\Form\SecurityUserPasswordType;
use App\Form\SecurityUserProfilType;
use App\Form\SecurityUserRoleType;
use App\Form\SecurityUserType;
use App\Services\SecurityService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class SecurityController extends AbstractController
{
    /**
     * @var SecurityServiceService
     */
    private $SecurityService;

    public function __construct(SecurityService $SecurityService)
    {
        $this->SecurityService = $SecurityService;
    }

    /**
     * @Route("/registration", name="registration")
     */
    public function registration(Request $request)
    {
        $SecurityUser = new SecurityUser();
        $form = $this->createForm(SecurityUserType::class, $SecurityUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $SecurityUser->setRole("ROLE_USER");
            $this->SecurityService->post($SecurityUser);
            return $this->redirect("/login");
        }
        return $this->render('security/registration.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/changUser/{idUser}", name="changUser")
     */
    public function changUser(Request $request,$idUser)
    {
        $SecurityUser = $this->SecurityService->get($idUser);

        $form = $this->createForm(SecurityUserProfilType::class, $SecurityUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->SecurityService->post($SecurityUser);
            return $this->redirect("/showProfil/$idUser");
        }
        return $this->render('security/registration.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/changRole/{idUser}", name="changRole")
     */
    public function changRole(Request $request,$idUser)
    {

        $SecurityUser = $this->SecurityService->get($idUser);

        $form = $this->createForm(SecurityUserRoleType::class, $SecurityUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->SecurityService->post($SecurityUser);
            return $this->redirect("/showProfil/$idUser");
        }
        return $this->render('security/registration.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/changPassword/{idUser}", name="changPassword")
     */
    public function changPassword(Request $request,$idUser)
    {

        $SecurityUser = $this->SecurityService->get($idUser);

        $form = $this->createForm(SecurityUserPasswordType::class, $SecurityUser);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->SecurityService->post($SecurityUser);
            return $this->redirect("/showProfil/$idUser");
        }
        return $this->render('security/registration.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/login", name="security_login")
     */
    public function login(AuthenticationUtils $authenticationUtils)
    {
        $error = $authenticationUtils->getLastAuthenticationError();

        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
        ));
    }

    /**
     * @Route("/logout", name="security_logout")
     */
    public function logout()
    {
        return $this->render('security/login.html.twig', [
            'controller_name' => 'SecurityController',
        ]);
    }

    /**
     * @Route("/showProfil/{idUser}", name="showProfil")
     */
    public function showProfil($idUser = null)
    {
            $user = $this->SecurityService->get($idUser);

        return $this->render('security/showProfil.html.twig', [
            'controller_name' => 'SecurityController',
            'user' => $user
        ]);
    }

    /**
     * @Route("/panelAdmin", name="panelAdmin")
     */
    public function panelAdmin()
    {
        $listUser = $this->SecurityService->getAll();
        return $this->render('security/panelAdmin.html.twig', [
            'controller_name' => 'SecurityController',
            'listUser' => $listUser,
        ]);
    }
}
