<?php

namespace App\Controller;

use App\Form\SteamIdType;
use App\Services\SteamService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class SteamController extends AbstractController
{
    /**
     * @var SteamService
     */
    private $steamService;
    /**
     * @var string
     */
    private $api_key;

    public function __construct(SteamService $steamService)
    {
        $this->api_key = "5EC29C48D828BCF4CD6FFE904881F6C0";
        $this->steamService = $steamService;
    }

    /**
     * @Route("/getSteamUser", name="getSteamUser")
     */
    public function getSteamUser(Request $request)
    {
        $form = $this->createForm(SteamIdType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $steamid = $form["Id"]->getViewData();
            $user = $this->steamService->getSteamUser($this->api_key, $steamid);

            return $this->render('steam/showSteamUser.html.twig', [
                'user' => $user
            ]);
        }
        return $this->render('steam/SteamId.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/getSteamGame", name="getSteamGame")
     */
    public function getSteamGame(Request $request)
    {
        $form = $this->createForm(SteamIdType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $steamid = $form["Id"]->getViewData();
            $game = $this->steamService->getSteamGame($steamid);

            return $this->render('steam/showSteamGame.html.twig',[
                'game' => $game
            ]);
        }
        return $this->render('steam/SteamId.html.twig', [
            'controller_name' => 'SecurityController',
            'form' => $form->createView(),
        ]);

    }

}