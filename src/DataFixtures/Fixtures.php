<?php

namespace App\DataFixtures;

use App\Entity\Game;
use App\Entity\Message;
use App\Entity\SecurityUser;
use App\Services\GameService;
use App\Services\MessageService;
use App\Services\SecurityService;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class Fixtures extends Fixture
{
    /**
     * @var SecurityService
     */
    private $SecurityService;
    /**
     * @var GameService
     */
    private $GameService;

    /**
     * @var MessageService
     */
    private $MessageService;

    public function __construct(SecurityService $SecurityService, GameService $gameService, MessageService $messageService)
    {
        $this->SecurityService = $SecurityService;
        $this->GameService = $gameService;
        $this->MessageService = $messageService;
    }

    public function load(ObjectManager $manager)
    {
        $toto = new SecurityUser();
        $toto ->setEmail("toto@toto.toto");
        $toto->setUsername("toto");
        $toto->setPassword("testtest");
        $toto ->setConfirmPassword($toto->getPassword());
        $toto->setRole("ROLE_USER");
        $this->SecurityService->post($toto);

        $admin = new SecurityUser();
        $admin ->setEmail("admin@admin.admin");
        $admin->setUsername("admin");
        $admin->setPassword("testtest");
        $admin ->setConfirmPassword($admin->getPassword());
        $admin->setRole("ROLE_ADMIN");
        $this->SecurityService->post($admin);

        $titi = new SecurityUser();
        $titi ->setEmail("titi@titi.titi");
        $titi->setUsername("titi");
        $titi->setPassword("testtest");
        $titi ->setConfirmPassword($titi->getPassword());
        $titi->setRole("ROLE_REDACTEUR");
        $this->SecurityService->post($titi);

        $game = new Game();
        $game->setName("League of Legend");
        $game->setDescription("un moba trés chronophage");
        $this->GameService->postGame($game);


        $message = new Message();
        $message->setSecurityUser($titi);
        $message->setGame($game);
        $message->setText("tro b1 ce je, jsu i bronse 3");
        $this->MessageService->postMessage($message);

        $message = new Message();
        $message->setSecurityUser($admin);
        $message->setGame($game);
        $message->setText("titi t'es viré");
        $this->MessageService->postMessage($message);

        $game = new Game();
        $game->setName("World of Warcraft");
        $game->setDescription("un mmorpg fort sympatique");
        $this->GameService->postGame($game);


        $message = new Message();
        $message->setSecurityUser($titi);
        $message->setGame($game);
        $message->setText("même un débutant peut progresser, le vrai jeux commence au lvl max");
        $this->MessageService->postMessage($message);

        $game = new Game();
        $game->setName("Age of empire");
        $game->setDescription("la référence de strategie ");
        $this->GameService->postGame($game);


        $message = new Message();
        $message->setSecurityUser($titi);
        $message->setGame($game);
        $message->setText("arreter de lire ce commentaire pour aller acheter le jeux");
        $this->MessageService->postMessage($message);
    }
}
