<?php

namespace App\Form;

use App\Entity\Game;
use App\Entity\Message;
use App\Repository\GameRepository;
use App\Services\GameService;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MessageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $gameService = $options['game_service'];

        $builder
            ->add('text')
            //->add('game')
            //->add('game', GameType::class, [])
            ->add('game', EntityType::class, [
                'class' => Game::class,
                'choices' => $gameService->getAllGame()
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setDefaults([
                'data_class' => Message::class,
            ])
            ->setRequired('game_service')
        ;
    }
}
