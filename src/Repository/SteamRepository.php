<?php

namespace App\Repository;

use App\Entity\Steam;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Steam|null find($id, $lockMode = null, $lockVersion = null)
 * @method Steam|null findOneBy(array $criteria, array $orderBy = null)
 * @method Steam[]    findAll()
 * @method Steam[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SteamRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Steam::class);
    }

    // /**
    //  * @return Steam[] Returns an array of Steam objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Steam
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
