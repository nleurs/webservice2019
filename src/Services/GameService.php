<?php

namespace App\Services;

use App\Entity\Game;
use App\Repository\GameRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;


class GameService
{


    /**
     * @var GameRepository
     */
    private $gameRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * GameService constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager, GameRepository $gameRepository)
    {
        $this->gameRepository = $gameRepository;
        $this->manager = $manager;
    }

    public function getAllGame()
    {
        return $this->gameRepository->findBy(array(), array('name' => 'asc'));
    }

    public function searchGame($name)
    {
        return $this->gameRepository->findOneBy(array("name" => $name));
    }

    public function getGame($id)
    {
        return $this->gameRepository->find($id);
    }

    public function postGame($game)
    {
        $this->manager->persist($game);
        $this->manager->flush();
    }

    public function delete($id)
    {
        $game = $this->gameRepository->find($id);
        $this->manager->remove($game);
        $this->manager->flush();
    }
}