<?php

namespace App\Services;

use App\Entity\Message;
use App\Repository\MessageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;


class MessageService
{


    /**
     * @var MessageRepository
     */
    private $messageRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * MessageService constructor.
     * @param EntityManagerInterface $manager
     * @param MessageRepository $messageRepository
     */
    public function __construct(EntityManagerInterface $manager, MessageRepository $messageRepository)
    {
        $this->messageRepository = $messageRepository;
        $this->manager = $manager;
    }

    public function getAllMessages()
    {
        return $this->messageRepository->findAll();
    }

    public function getMessage($id)
    {
        return $this->messageRepository->find($id);
    }

    public function postMessage($message)
    {
        $this->manager->persist($message);
        $this->manager->flush();
    }

    public function delete($id)
    {
        $message = $this->messageRepository->find($id);
        $this->manager->remove($message);
        $this->manager->flush();
    }
}