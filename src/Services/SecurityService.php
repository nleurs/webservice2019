<?php

namespace App\Services;

use App\Entity\SecurityUser;
use App\Repository\SecurityUserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class SecurityService
{


    /**
     * @var SecurityUserRepository
     */
    private $SecurityUserRepository;
    /**
     * @var EntityManagerInterface
     */
    private $manager;

    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    /**
     * SecurityService constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(EntityManagerInterface $manager, SecurityUserRepository $SecurityUserRepository, UserPasswordEncoderInterface $encoder)
    {
        $this->SecurityUserRepository = $SecurityUserRepository;
        $this->manager = $manager;
        $this->encoder = $encoder;
    }

    public function getAll()
    {
        return $this->SecurityUserRepository->findBy(array(),array('username' => 'asc'));
    }

    public function get($id)
    {
        return $this->SecurityUserRepository->find($id);
    }


    public function post($SecurityUser)
    {
        $hash = $this->encoder->encodePassword($SecurityUser, $SecurityUser->getPassword());
        $SecurityUser->setPassword($hash);
        $this->manager->persist($SecurityUser);
        $this->manager->flush();
    }

    public function delete($id)
    {
        $SecurityUser = $this->SecurityUserRepository->find($id);
        $this->manager->remove($SecurityUser);
        $this->manager->flush();
    }
}