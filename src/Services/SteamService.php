<?php

namespace App\Services;

use App\Entity\Steam;
use App\Repository\SteamRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManagerInterface;

class SteamService
{
    public function getSteamUser($api_key, $steamid)
    {
        $api_url = "http://api.steampowered.com/ISteamUser/GetPlayerSummaries/v0002/?key=$api_key&steamids=$steamid";


        $json = json_decode(file_get_contents($api_url));
        if($json->response->players==null)
            return false;
        return $json->response->players[0];

    }
    public function getSteamGame($steamid)
    {
        $api_url = "http://store.steampowered.com/api/appdetails?appids=$steamid";
        $json = json_decode(file_get_contents($api_url));

        return $json->$steamid->data;

    }

}